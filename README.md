This repository contains the contracts for the Omnisea Omnichain NFTs Gateway.
For the Omnichain Router contracts responsible for cross-chain messaging, see the [omnichain-router](https://gitlab.com/omnisea/omnichain-router)
repository.

## Licensing

The primary license for Omnisea Contracts is the Business Source License 1.1 (`BUSL-1.1`), see [`LICENSE`](./LICENSE).
