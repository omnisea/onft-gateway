const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'moonbeam_MAINNET';
const networkToDeployParamsMap = {
    // rinkeby: {
    //     router: '0x9723BD2f094D3943533fbf0dF04EB2C791058c46',
    // },
    // bsc_testnet: {
    //     router: '0x862929656A7B975734C1374F8DC29EeFff1626F2',
    // },
    // avalanche_testnet: {
    //     router: '0xF8780698D4a5dC67c9a94F225450dB354E84e8a4',
    // },
    // polygon_testnet: {
    //     router: '0x1A7Af8295581B268367e4E43Df89d8cfC1B7A72d',
    // },
    // arbitrum_testnet: {
    //     router: '0x38401204ab46600BCB07C3060d743EBB15D7707f',
    // },
    // optimism_testnet: {
    //     router: '0x59e10621aE1Ddc9D9f15c0559bAEbc4c7837A959',
    // },
    // ftm_testnet: {
    //     router: '0x9A1e197b50B8C5993F5e23416159b5Ee5E18476b',
    // },
    // moonbeam_testnet: {
    //     router: '0x53b602a33564E828D1ABF1dD44b683761DCD42Eb',
    // },

    // !!! MAINNET NETWORKS !!!

    mainnet: {
        router: '0x1dE48d49aeBa6Ffa4740e78c84a13de8a9c12911',
    },
    bsc_MAINNET: {
        router: '0x7E3A34C040956C6fC8B1231ab53B355998367563',
    },
    avalanche_MAINNET: {
        router: '0xEeb51a31685bf7385B0825139320C13Dce16f5Fc',
    },
    polygon_MAINNET: {
        router: '0x077668085D7ba60832B7227E935C90C756501A0E',
    },
    arbitrum_MAINNET: {
        router: '0xb9fd670E32B51188B9C44C2E2226Ec345F4debDc',
    },
    optimism_MAINNET: {
        router: '0x5ee0753e3aB9a991849200Ff70B342053b61ef6D',
    },
    ftm_MAINNET: {
        router: '0xF590d2958D557b98EF2c799813A2B35Bc1cEF4e4',
    },
    harmony_MAINNET: {
        router: '0xde0F6167cec79A127e6973dAa0923aD1E87b8BCC',
    },
    moonbeam_MAINNET: {
        router: '0x89557E29812f1967dd40E087A9f8BA0073B5DD8A',
    },
};

async function main() {
    const networkParams = networkToDeployParamsMap[network];

    if (!networkParams) {
        throw new Error('Unknown network');
    }
    console.log(`Deploying ONFTGateway for !!! ${network} !!!`);
    const ONFTGateway = await hre.ethers.getContractFactory("ONFTGateway");
    const contract = await ONFTGateway.deploy(networkParams.router);
    await contract.deployed();
    console.log("ONFTGateway deployed to:", contract.address);

    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: contract.address,
                        constructorArguments: [
                            networkParams.router,
                        ],
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 45000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });

//
