const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const network = 'Polygon';
const oaMap = {
    Ethereum: '',
    BSC: '',
    Avalanche: '',
    Polygon: '',
    Arbitrum: '',
    Optimism: '',
    Fantom: '',
    Harmony: '',
    Moonbeam: '',
};

async function main() {
    console.log(`Setting ONFTGateway OA for !!! ${network} !!!`);
    const contract = await hre.ethers.getContractAt(
        ["function setOA(string memory remoteChainName, address remoteOA) external"],
        oaMap[network],
        (await hre.ethers.getSigners())[0],
    );

    for (const chainName in oaMap) {
        if (!oaMap[chainName] || chainName === network) {
            console.log(`Ignoring ${chainName}`);
            continue;
        }
        await (await contract.setOA(chainName, oaMap[chainName])).wait();
        console.log(`Set OA for ${chainName}: ${oaMap[chainName]}`);
    }
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });

//
