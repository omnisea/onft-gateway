const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    const testERC721 = await hre.ethers.getContractFactory("TestERC721");
    const contract = await testERC721.deploy();
    await contract.deployed();
    console.log("TestERC721 deployed to:", contract.address);

    await contract.mint((await hre.ethers.getSigners())[0].address, 1);
    await contract.mint((await hre.ethers.getSigners())[0].address, 2);

    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: contract.address,
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 45000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });

//