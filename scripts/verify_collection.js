const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    await (() => {
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
                try {
                    await hre.run("verify:verify", {
                        address: "0x73db0ac0da2420aab87fe83afee724ce5f8e6c38",
                        constructorArguments: [
                            "Primate Social Society",
                            "PSS",
                            "0x1634b10e46D6323a6c8b740775410ef7fA43598c",
                        ],
                    });

                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, 1000);
        });
    })();
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });