const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    const exampleNft = await hre.ethers.getContractAt(
        ["function burn(uint256 tokenId) external"],
        "0xFcCb8d410c71C3E119239296dDe57089a5B7e929", // collection
        (await hre.ethers.getSigners())[0],
    );

    // tokenId
    await exampleNft.burn(1);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });