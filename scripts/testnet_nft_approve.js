const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

async function main() {
    const exampleNft = await hre.ethers.getContractAt(
        ["function approve(address to, uint256 tokenId) public"],
        "0x757A5aA4Cf8AC54428321AE49fD81204A7140EBA", // collection
        (await hre.ethers.getSigners())[0],
    );

    // ONFTGateway, tokenId
    await exampleNft.approve("0x556E4191AD4ca6436CAB6068d828232CD0DD1aeA", 1);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });