// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

struct ONFTLock {
    address collection;
    uint256 tokenId;
    address owner;
    uint256 at;
}
