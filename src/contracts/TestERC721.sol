// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract TestERC721 is ERC721 {
    constructor(
    ) ERC721("Test", "TST") {}

    function burn(uint256 tokenId) external {
        _burn(tokenId);
    }

    function mint(address owner, uint256 tokenId) external {
        _safeMint(owner, tokenId);
    }
}
